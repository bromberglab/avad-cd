#!/bin/bash

print_usage() {
  printf "\nUsage: ./avadx.sh -i sample01.exonic_variant_function -o output_folder/ -t annovar \nOptions:\n\t-i    The path to the input file\n\t-o    The path to the output file folder\n\t-t    Type of the input file. annovar: exonic_variant_function output file from annovar; vcf: a VCF file\n\n"
}

while getopts "i:o:t:" option; do
  case "${option}" in
    i) i_flag="${OPTARG}" ;;
    o) o_flag="${OPTARG}" ;;
    t) t_flag="${OPTARG}" ;;
    h) print_usage
       exit 0
       ;;
    \?) 
       print_usage
       exit 1
       ;;
  esac
done
shift $((OPTIND -1))


#echo ${i_flag}
#echo ${o_flag}
#echo ${t_flag}

f="$(basename -- $i_flag)"

#wd_exec=`pwd`
echo "Running AVA,Dx for input: ${i_flag}"

if [ "${t_flag}" = "annovar" ]
then
  # calculate gene score:
  docker run \
    --rm \
    -v "${i_flag}":/home/mount/"${f}" \
    -v "${o_flag}":/home/mount \
    analysis-avadx:latest Rscript /home/analysis/R/Gene_Score.R -f /home/mount/"${f}" -m /home/analysis/resources/db/Uniprot_20150925.xml.txt -s /home/analysis/resources/db/SnapScores.txt -o /home/mount/"${f}".gene_score
  # > ${logfile}
  rm ${o_flag}/${f}
  echo "gene_score file is at: ${o_flag}/${f}.gene_score"

  # prediction:
  docker run \
    --rm \
    -v "${o_flag}":/home/mount \
    analysis-avadx:latest Rscript /home/analysis/R/Prediction.R -f /home/mount/"${f}".gene_score -o /home/mount/"${f}".prediction

  echo "CD prediction file is at: ${o_flag}/${f}.prediction"
elif [ "${t_flag}" = "vcf" ]
then
  # clean VCF file:
  docker run \
    --rm \
    -v "${i_flag}":/home/mount/"${f}" \
    -v "${o_flag}":/home/mount/out \
    analysis-avadx:latest vcftools --vcf /home/mount/"${f}" --remove-indels --recode --out /home/mount/out/"${f%.*}" > /dev/null 2>&1
  echo "Finished running VCFtools; file at: ${o_flag}/${f%.*}.recode.vcf"

  # run ANNOVAR step1:
  docker run \
    --rm \
    -v "${o_flag}":/home/mount/out \
    analysis-avadx:latest /home/analysis/binaries/annovar/convert2annovar.pl -format vcf4 /home/mount/out/"${f%.*}".recode.vcf -outfile /home/mount/out/sample -allsample > /dev/null 2>&1
  echo "Finished running ANNOVAR (prepare input file)"

  # run ANNOVAR step2 for every sample:
  for f_avinput in "${o_flag}"/*.avinput
  do
    docker run \
      --rm \
      -v "${o_flag}":/home/mount/out \
      analysis-avadx:latest /home/analysis/binaries/annovar/annotate_variation.pl -build hg19 /home/mount/out/"$(basename -- $f_avinput)" /home/analysis/binaries/annovar/humandb/ > /dev/null 2>&1
  done
  echo "Finished running ANNOVAR (gene-based annotation)" # Slow step with Docker

  # calculate gene score for every sample:
  for f_exonic in "${o_flag}"/*exonic_variant_function
  do
    #echo "$(basename -- $f_exonic)"
    docker run \
      --rm \
      -v "${o_flag}":/home/mount/out \
      analysis-avadx:latest Rscript /home/analysis/R/Gene_Score.R -f /home/mount/out/"$(basename -- $f_exonic)" -m /home/analysis/resources/db/Uniprot_20150925.xml.txt -s /home/analysis/resources/db/SnapScores.txt -o /home/mount/out/"$(basename -- $f_exonic)".gene_score
  done
  echo "Finished calculating gene score" 

  #prediction for every sample:
  for f_genescore in "${o_flag}"/*gene_score
  do
    docker run \
      --rm \
      -v "${o_flag}":/home/mount/out \
      analysis-avadx:latest Rscript /home/analysis/R/Prediction.R -f /home/mount/out/"$(basename -- $f_genescore)" -o /home/mount/out/"$(basename -- $f_genescore)".prediction
  done
  echo "Predicted CD predisposition risk file at: ${o_flag}/$(basename -- $f_genescore).prediction"
fi
