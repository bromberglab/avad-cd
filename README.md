# AVA,Dx #
### Analysis of Variation for Association with Disease x###

* A pipeline that annotates gene function changes in next generation sequencing data (VCF format or ANNOVAR .exonic_variant_function format), an SVM model for disease predisposition prediction of Crohn's Disease (CD).
* We are currently testing (1) different gene scoring schemes for better gene features; (2) on data from other complex diseases.

* AVA,Dx currently uses a panel of 111 individuals from whole exome sequencing to train the model. Overlapping more variant loci with this panel is expected to generate more reliable predictions.
* AVA,Dx currently only takes hg19 (GRCh37) reference assembly version.
* The input for the pipeline can be a VCF file or .exonic_variant_function file from ANNOVAR annotation.

## Pre-Requirements ##
* Docker (https://www.docker.com)
* ANNOVAR (optional)

A docker image of AVA,Dx that predicts the CD predisposition of exome/genome data is available by:
```
docker pull bromberglab/avadx
```

Run AVA,Dx (input: .exonic_variant_function file) by:
```
./avadx.sh -t annovar -i /path/to/file_sample001.exonic_variant_function -o /path/to/output_folder
```
Run AVA,Dx (input: VCF file) by:
```
./avadx.sh -t vcf -i /path/to/input_file.vcf -o /path/to/output_folder
```
The the prediction result and intermediate files will be stored in the designated output folder after ```-o```.

---
## Reference/Citation ##
Wang, Yanran, Maximilian Miller, Yuri Astrakhan, Britt-Sabina Petersen, Stefan Schreiber, Andre Franke, and Yana Bromberg. "Identifying Crohn�s disease signal from variome analysis." Genome Medicine 11, no. 1 (2019): 59.

---
## VCF data for training ##
AVA,Dx is trained on a whole exome sequencing (WES) cohort of 64 CD and 47 healthy control individuals. We are currently working on increasing the training data size.

The individuals were collected from from Institute of Clinical Molecular Biology, Christian-Albrechts-University of Kiel, Germany (all are EUR referenced with 1000Genomes data) .

---
## Variant annotation ##
ANNOVAR was used for annotation of all variants.

http://annovar.openbioinformatics.org/

Output from ANNOVAR gene-based annotation (.exonic\_variant\_function file) is recommended to be used as input for our method.

---
## Batch effect removal ##
AVA,Dx takes the exonic variants from the input individual and compare the gene\_score of him/her with all individuals from the training set **_regardless_** of the disease label.

ComBat was used for the mean value adjustment of the gene_scores.
_**Reference**: Johnson, W. E., Li, C., & Rabinovic, A. (2007). Adjusting batch effects in microarray expression data using empirical Bayes methods. Biostatistics, 8(1), 118-127._



