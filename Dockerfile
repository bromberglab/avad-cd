FROM rocker/r-ver:3.3.3
MAINTAINER Yanran Wang <ywang@bromberglab.org>

ARG WHEN

# ensure UTF-8
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# setup
ENV HOME /root

# create analysis directory
RUN mkdir /home/analysis

# copy R directory
ADD build/R /home/analysis/R

# copy resources directory
ADD resources /home/analysis/resources

# create setup directory and 
RUN mkdir /home/setup

# copy setup file
ADD build/setup.sh /home/setup/setup.sh

# setup system
RUN DEBIAN_FRONTEND="noninteractive" apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y upgrade
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install libxml2-dev
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install libcurl4-gnutls-dev

RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('e1071')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('stringi')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('stringr')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('reshape2')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('optparse')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('data.table')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('dplyr')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('XML')"
RUN R -e "options(repos = \
  list(CRAN = 'http://mran.revolutionanalytics.com/snapshot/${WHEN}')); \
  install.packages('RCurl')"

# run setup file
RUN (cd /home/setup/; ./setup.sh)


