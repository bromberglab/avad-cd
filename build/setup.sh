#!/usr/bin/env bash

##-------------------------------------------------------
# PROJECT SETUP
##-------------------------------------------------------

# set UTF-8 environment
echo 'LC_ALL=en_US.UTF-8' >> /etc/environment
echo 'LANG=en_US.UTF-8' >> /etc/environment
echo 'LC_CTYPE=en_US.UTF-8' >> /etc/environment

# install R sva package:
Rscript /home/analysis/R/install_sva.R

